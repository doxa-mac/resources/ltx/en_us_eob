
# ltx en_us_eob 
Liturgical translation files used by Doxa when generating a liturgical website.

The files are © © Laurent Cleenewerck, Editor, 2007-2011. All rights reserved. Permissions to use, quote, reproduce and modify for non-commercial, liturgical or scholarly purposes is hereby granted to all institutions, parishes, clergy, or lay members affiliated to the affiliated jurisdictions and agencies of the Assembly of Canonical Orthodox Bishops of North and Central America, as well as all jurisdictions in communion with the Ecumenical Patriarchate of Constantinople. This clause applies to the biblical text and footnotes only, not to Introductory. It is requested that any modifications be communicated to: eobeditor@easternorthodoxbible.org for possible inclusion in future editions of the EOB.

The files in this repository are formatted for use with Doxa.

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
